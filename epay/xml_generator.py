import xml.etree.ElementTree as ET

def render_xml(context):
    merchant = ET.Element('merchant')
    merchant.set('cert_id',context['MERCHANT_CERTIFICATE_ID'])
    merchant.set('name', context['MERCHANT_NAME'])

    order = ET.SubElement(merchant, 'order')
    order.set('order_id', str(context['ORDER_ID']))
    order.set('amount', str(context['AMOUNT']))
    order.set('currency', str(context['CURRENCY']))

    department = ET.SubElement(order, 'department')
    department.set('merchant_id', context['MERCHANT_ID'])
    department.set('amount', str(context['AMOUNT']))

    if context['INSTALMENT']:
        department.set('instalment', context['INSTALMENT'])
        department.set('inst_period', context['INST_PERIOD'])

    result = str(ET.tostring(merchant).decode())
    return result