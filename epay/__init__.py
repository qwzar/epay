import os
from epay.exeptions import ImproperlyConfigured
from epay.process import get_context, postlink_process as postlink

version_info = (0, 0, 1)
__version__ = version = '.'.join(map(str, version_info))
__project__ = PROJECT = 'epay'
__author__ = AUTHOR = "qwazaar <eldar@unie.kz>"

def check_settings():
	if not os.environ.get('MERCHANT_ID'):
		raise ImproperlyConfigured('MERCHANT_ID is required')

	if not os.environ.get('MERCHANT_CERTIFICATE_ID'):
		raise ImproperlyConfigured('MERCHANT_CERTIFICATE_ID is required')

	if not os.environ.get('MERCHANT_NAME'):
		raise ImproperlyConfigured('MERCHANT_NAME is required')

	if not os.environ.get('PRIVATE_KEY_FN'):
		raise ImproperlyConfigured('PRIVATE_KEY_FN is required')

	if not os.environ.get('PRIVATE_KEY_PASS'):
		raise ImproperlyConfigured('PRIVATE_KEY_PASS is required')

	if not os.environ.get('PUBLIC_KEY_FN'):
		raise ImproperlyConfigured('PUBLIC_KEY_FN is required')

	os.environ["XML_TEMPLATE_FN"] = os.environ.get('XML_TEMPLATE_FN') or 'epay/template.xml'
	os.environ["XML_COMMAND_TEMPLATE_FN"] = os.environ.get('XML_COMMAND_TEMPLATE_FN') or 'epay/command_template.xml'